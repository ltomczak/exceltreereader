package model

/**
 * Klasa opisująca węzeł zawierająca dodatkowe informacje takie jak poziom i liczbę porządkową
 * z oryginalnego źródła danych.
 * @author Łukasz Tomczak.
 */
case class IndexedNode(idx: Int, lvl: Int, node: Node) {

  /**
   * Dodaje węzły potomne.
   * @param idxNodes węzły do dodania
   * @return wynikowy węzeł
   */
  def addChildrens(idxNodes: List[IndexedNode]): IndexedNode = {
    val nodes = for(n <-idxNodes) yield n.node
    IndexedNode(idx, lvl, node.addChildrens(nodes))
  }
}

/**
 * Obiekt pomocniczy dla funkcji operujących na obiektach typu IndexedNode.
 */
object IndexedNode {

  /**
   * Tworzy listę węzłów z listy obiektów typu IndexedNodes.
   * @param indexedNodes lista węzłów typu IndexedNode
   * @return lista węzłów typu Node z uzupełnionymi relacjami dziecko - rodzic
   */
  def getNodes(indexedNodes: List[IndexedNode]): List[Node] = {
    val roots = IndexedNode.getRoots(indexedNodes)
    val buildedIndexedNodes = buildTreeStructure(roots)
    buildedIndexedNodes.map(_.node)
  }

  /**
   * Tworzy korzenie struktury drzewiastej z listy obiektów typu IndexedNode.
   * @param allNodes lista wszystkich węzłów
   * @return lista korzeni struktury wraz z węzłami potomnymi
   */
  private def getRoots(allNodes: List[IndexedNode]): List[List[IndexedNode]] = allNodes match {
    case Nil => List()
    case _ =>
      val (r, l) = allNodes.splitAt(allNodes.lastIndexWhere(n => n.lvl == 0))
      List(l) ::: getRoots(r)
  }

  /**
   * Buduje strukturę drzewa.
   * @param rootNodes lista węzłów typu korzeń wraz z dziećmi
   * @return lista obiektów typu IndexedNode z uzupełnionymi relacjami dziecko - rodzic
   */
  private def buildTreeStructure(rootNodes: List[List[IndexedNode]]): List[IndexedNode] = {

    /**
     * Łączy listy podrzędne węzłów znajdujących się na tym samym poziomie w jedną listę.
     * @param stack lista list węzłów na tym samym poziomie w drzewie
     * @return znormalizowana lista węzłów podrzędnych
     */
    def normalizeStack(stack: List[List[IndexedNode]]): List[List[IndexedNode]] = {
      stack.foldRight(List[List[IndexedNode]]())((elem, acc) => {
        if (acc.isEmpty) List(elem) ::: acc
        else if (elem.head.lvl == acc.head.head.lvl) List(List(elem.head) ::: acc.head) ::: acc.tail
        else List(elem) ::: acc
      })
    }

    /**
     * Tworzy strukturę pojedynczego węzła typu korzeń.
     * @param nodes lista węzłów
     * @param stack akumulator
     * @return lista węzłów z uzupełnionymi relacjami dziecko - rodzic
     */
    def loop(nodes: List[IndexedNode], stack: List[List[IndexedNode]]): List[IndexedNode] = {
      val normalisedStack: List[List[IndexedNode]] = normalizeStack(stack)
      if (nodes.isEmpty) normalisedStack.head
      else if (normalisedStack.isEmpty) loop(nodes.tail, List(List(nodes.head)) ::: normalisedStack)
      else if (nodes.head.lvl == normalisedStack.head.head.lvl) loop(nodes.tail, List(List(nodes.head) ::: normalisedStack.head) ::: normalisedStack.tail)
      else if (nodes.head.lvl < normalisedStack.head.head.lvl) loop(nodes.tail, List(List(nodes.head.addChildrens(normalisedStack.head))) ::: normalisedStack.tail)
      else loop(nodes.tail, List(List(nodes.head)) ::: normalisedStack)
    }

    (for {
      node <- rootNodes
      nodesObj = loop(node.reverse, List())
    } yield nodesObj).flatten
  }
}
