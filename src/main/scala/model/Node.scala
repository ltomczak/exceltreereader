package model

import argonaut._, Argonaut._

/**
 * Klasa opisująca węzeł drzewa.
 * @author Łukasz Tomczak.
 */
case class Node(id: Int, name: String, nodes: List[Node]) {

  /**
   * Dodaje węzły podrzędne.
   * @param node lista węzłów podrzędnych
   * @return wynikowy węzeł
   */
  def addChildrens(node: List[Node]): Node = {
    Node(id, name, nodes ::: node)
  }
}

/**
 * Obiekt pomocniczy dla klasy Node.
 */
object Node {
  /**
   * Implementacja encodera do JSON'a.
   * @return encoder
   */
  implicit def NodeEncodeJson: EncodeJson[Node] = EncodeJson((n: Node) => ("id" := n.id) ->: ("name" := n.name) ->: ("nodes" := n.nodes) ->: jEmptyObject)
}
