import info.folone.scala.poi.impure.load
import info.folone.scala.poi._
import model.{Node, IndexedNode}

import scala.collection.Set

/**
 * Pomocniczy singleton do operacji na arkuszu kalkulacyjnym.
 *
 * @author Łukasz Tomczak.
 */
object XLSUtil {

  /**
   * Pobiera wiersze z arkusza kalkulacyjnego w formie listy obiektów typu IndexedNode.
   * @param filePath scieżka do pliku
   * @return lista obiektów typu IndexedNode
   */
  def getIndexedNodesFromFile(filePath: String): List[IndexedNode] = {
    val nodeRows = XLSUtil.getRowsFromFile(filePath)
    getSortedRowsWithoutLabel(nodeRows).map(convertRowToIndexedNode)
  }

  /**
   * Pobiera dane z arkusza kalkulacyjnego.
   * @param filePath scieżka do pliku
   * @return zbiór wierszy z arkusza numer 1
   */
  private def getRowsFromFile(filePath: String): Set[Row] = {
    val workbook = load(filePath)
    workbook.sheets.head.rows
  }

  /**
   * Sortuje zbiór wierszy po id  wiersza.
   * @param rows zbiór wierszy do posortowania
   * @return posortowany zbiór wierszy
   */
  private def getSortedRowsWithoutLabel(rows: Set[Row]): List[Row] = {
    val sortedRowsByIdx = rows.toList.sortBy(row => row.index)
    sortedRowsByIdx.tail
  }

  /**
   * Konwertuje wiersz z arkusza kalkulacyjnego do obiektu typu IndexedNode.
   * @param row wiersz z danymi
   * @return obiekt IndexedNode odpowiadający danemu wierszowi
   */
  private def convertRowToIndexedNode(row: Row): IndexedNode = {
    val Row(idx, cells) = row
    val List(StringCell(level, nodeName), NumericCell(_, nodeIdx)) = cells.toList
    IndexedNode(idx, level, Node(nodeIdx.toInt, nodeName, List()))
  }

}
