import model.IndexedNode
import argonaut._, Argonaut._

/**
 * Główny obiekt programu.
 * @author Łukasz Tomczak.
 */
object TreeReader {
  /**
   * Wypisuje na standardowe wyjście strukturę drzewiastą w formacie JSON na podstawie definicji w pliku XLS.
   * @param args scieżka do pliku xls
   */
  def main(args: Array[String]) {
    val indexedNodes = XLSUtil.getIndexedNodesFromFile(args(0))
    println(IndexedNode.getNodes(indexedNodes).asJson.spaces2)
  }
}
