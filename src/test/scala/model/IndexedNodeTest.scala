package model

import model.IndexedNode
import org.junit.runner.RunWith
import org.scalatest.FunSuite

/**
 * Test klasy IndexedNode
 * @author Łukasz Tomczak.
 */
class IndexedNodeTest extends FunSuite {

  test("One elem")(IndexedNode.getNodes(List(IndexedNode(1, 0, Node(33, "a", Nil)))) === List(Node(33, "a", Nil)))

  test("One child")(IndexedNode.getNodes(List(IndexedNode(1, 0, Node(33, "a", Nil)), IndexedNode(2, 1, Node(43, "aa", Nil))))
        === List(Node(33, "a", List(Node(43, "aa", Nil)))))

  test("Nil")(IndexedNode.getNodes(Nil) === Nil)

  test("Example From File")(
    IndexedNode.getNodes(
      List(
        IndexedNode(1, 0, Node(11, "a", Nil)),
        IndexedNode(2, 1, Node(12, "aa", Nil)),
        IndexedNode(3, 2, Node(13, "aa1", Nil)),
        IndexedNode(4, 2, Node(14, "aa2", Nil)),
        IndexedNode(5, 1, Node(15, "ab", Nil)),
        IndexedNode(6, 2, Node(16, "ab1", Nil))
      )) === List(Node(11, "a", List(Node(12, "aa", List(Node(13, "aa1", Nil), Node(14, "aa2", Nil))), Node(15, "ab", List(Node(16, "ab1", Nil)))))))

}
