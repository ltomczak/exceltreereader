name := "TreeReader"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "info.folone" %% "poi-scala" % "0.14"

//milestone ze względu na obsługę scalaz-7.1
libraryDependencies += "io.argonaut" %% "argonaut" % "6.1-M4" changing()

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

libraryDependencies += "junit" % "junit" % "4.10" % "test"


    